extern crate core;

use std::fs;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Choice {
    Rock,
    Paper,
    Scissors,
}

impl Choice {
    fn from_char(c: char) -> Self {
        match c {
            'A' => Self::Rock,
            'B' => Self::Paper,
            'C' => Self::Scissors,
            'X' => Self::Rock,
            'Y' => Self::Paper,
            'Z' => Self::Scissors,
            _ => todo!(),
        }
    }

    fn value(&self) -> u32 {
        match self {
            Choice::Rock => 1,
            Choice::Paper => 2,
            Choice::Scissors => 3,
        }
    }

    fn loses_from(&self) -> Self {
        match self {
            Choice::Rock => Choice::Paper,
            Choice::Paper => Choice::Scissors,
            Choice::Scissors => Choice::Rock,
        }
    }
}

#[derive(Debug)]
enum Outcome {
    Win,
    Lose,
    Draw,
}

impl Outcome {
    fn value(&self) -> u32 {
        match self {
            Outcome::Win => 6,
            Outcome::Lose => 0,
            Outcome::Draw => 3,
        }
    }

    fn from_char(c: char) -> Self {
        match c {
            'X' => Outcome::Lose,
            'Y' => Outcome::Draw,
            'Z' => Outcome::Win,
            _ => todo!(),
        }
    }
}

#[derive(Debug)]
struct Round {
    opponent: Choice,
    you: Choice,
}

impl Round {
    fn from_str(text: &str) -> Self {
        Self {
            opponent: Choice::from_char(text.chars().nth(0).unwrap()),
            you: Choice::from_char(text.chars().nth(2).unwrap()),
        }
    }

    fn from_str2(text: &str) -> Self {
        let outcome = Outcome::from_char(text.chars().nth(2).unwrap());
        let opponent = Choice::from_char(text.chars().nth(0).unwrap());
        let you = Self::my_move(opponent, outcome);
        Self { opponent, you }
    }

    fn value(&self) -> u32 {
        self.outcome().value() + self.you.value()
    }

    fn my_move(opponent: Choice, outcome: Outcome) -> Choice {
        match outcome {
            Outcome::Win => opponent.loses_from(),
            Outcome::Lose => opponent.loses_from().loses_from(),
            Outcome::Draw => opponent,
        }
    }

    fn outcome(&self) -> Outcome {
        if self.opponent == self.you {
            return Outcome::Draw;
        }
        if self.opponent.loses_from() == self.you {
            Outcome::Win
        } else {
            Outcome::Lose
        }
    }
}

fn calculate(data: &str) -> u32 {
    let rounds: Vec<Round> = data.lines().map(Round::from_str).collect();
    let outcome: u32 = rounds.iter().map(|round| round.value()).sum();
    outcome
}

fn calculate2(data: &str) -> u32 {
    let rounds: Vec<Round> = data.lines().map(Round::from_str2).collect();
    let outcome: u32 = rounds.iter().map(|round| round.value()).sum();
    outcome
}

fn main() {
    let data = fs::read_to_string("input.txt").unwrap();
    let outcome = calculate(&data);
    println!("question 1: {}", outcome);
    let outcome = calculate2(&data);
    println!("question 2: {}", outcome);
}

#[cfg(test)]
mod test {
    use crate::{calculate, calculate2};

    #[test]
    fn test1() {
        let data = "A Y
B X
C Z";
        let outcome = calculate(data);
        assert_eq!(outcome, 15);
    }
    #[test]
    fn test2() {
        let data = "A Y
B X
C Z";
        let outcome = calculate2(data);
        assert_eq!(outcome, 12);
    }
}
