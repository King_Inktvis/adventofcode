use std::fs;

struct Range {
    start: u32,
    end: u32,
}

impl Range {
    fn from_str(text: &str) -> Self {
        let (num1, num2) = text.split_once("-").unwrap();
        Self {
            start: num1.parse().unwrap(),
            end: num2.parse().unwrap(),
        }
    }

    fn in_range(&self, num: u32) -> bool {
        num >= self.start && num <= self.end
    }

    fn contains(&self, comparison: &Range) -> bool {
        self.in_range(comparison.start) && self.in_range(comparison.end)
    }

    fn has_overlap(&self, comparison: &Range) -> bool {
        self.in_range(comparison.start)
            || self.in_range(comparison.end)
            || comparison.in_range(self.start)
            || comparison.in_range(self.end)
    }
}

fn convert_line(line: &str) -> (Range, Range) {
    let (task1, task2) = line.split_once(",").unwrap();
    let range1 = Range::from_str(task1);
    let range2 = Range::from_str(task2);
    (range1, range2)
}

fn main() {
    let data = fs::read_to_string("input.txt").unwrap();
    let outcome = data
        .lines()
        .map(|line| {
            let (range1, range2) = convert_line(line);
            range1.contains(&range2) || range2.contains(&range1)
        })
        .filter(|v| *v)
        .count();
    println!("answer 1: {}", outcome);

    let outcome2 = data
        .lines()
        .map(|line| {
            let (range1, range2) = convert_line(line);
            range1.has_overlap(&range2)
        })
        .filter(|v| *v)
        .count();
    println!("answer 2: {}", outcome2);
}
