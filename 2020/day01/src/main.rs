use std::fs;

fn main() {
    let data = fs::read_to_string("input.txt").unwrap();
    let elves_list = get_elves(data);
    let mut calorie_list: Vec<u32> = elves_list.iter().map(|e| elf_calories(e)).collect();
    calorie_list.sort();

    println!("highest elf calories: {}", calorie_list.last().unwrap());
    let top3: u32 = calorie_list[calorie_list.len() - 3..calorie_list.len()]
        .iter()
        .sum();

    println!("highest 3 elf calories: {}", top3);
}

fn get_elves(data_string: String) -> Vec<String> {
    data_string.split("\n\n").map(|s| s.to_string()).collect()
}

fn elf_calories(elf: &str) -> u32 {
    elf.split("\n").map(|n| n.parse::<u32>().unwrap()).sum()
}
