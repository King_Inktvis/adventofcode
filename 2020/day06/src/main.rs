extern crate core;

use std::collections::HashSet;
use std::fs;

fn main() {
    let text = fs::read_to_string("input.txt").unwrap();
    let data: Vec<char> = text.chars().collect();

    println!("answer 1: {}", find_message_start(&data, 4));
    println!("answer 2: {}", find_message_start(&data, 14));
}

fn find_message_start(data: &Vec<char>, size: usize) -> usize {
    for i in 0..(data.len() - size) {
        let slice = &data[i..(i + size)];
        if is_all_unique(slice) {
            return i + size;
        }
    }
    panic!();
}

fn is_all_unique(input: &[char]) -> bool {
    let mut set = HashSet::new();
    for c in input {
        if !set.insert(c) {
            return false;
        }
    }
    true
}
