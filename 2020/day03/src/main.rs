use itertools::Itertools;
use std::fs;

fn main() {
    let data = fs::read_to_string("input.txt").unwrap();
    let outcome: u16 = data
        .lines()
        .map(|line| {
            let (pack1, pack2) = line.split_at(line.len() / 2);
            let wrong_item = pack1.chars().find(|c| pack2.contains(*c)).unwrap();
            map_char_to_value(wrong_item)
        })
        .sum();
    println!("answer 1: {:?}", outcome);

    let outcome2: u16 = data
        .lines()
        .chunks(3)
        .into_iter()
        .map(|elfs| {
            let elfs: Vec<&str> = elfs.collect();
            let common_item = elfs[0]
                .chars()
                .find(|c| elfs[1].contains(*c) && elfs[2].contains(*c))
                .unwrap();
            map_char_to_value(common_item)
        })
        .sum();
    println!("answer 2: {}", outcome2);
}

fn map_char_to_value(c: char) -> u16 {
    let number = c as u16;
    let detract = if c.is_uppercase() { 38 } else { 96 };
    number - detract
}
