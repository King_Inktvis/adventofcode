use std::fs;

#[derive(Debug)]
struct Stack(Vec<char>);

impl Stack {
    fn new() -> Self {
        Self(Vec::new())
    }
    fn push(&mut self, c: char) -> &Self {
        self.0.push(c);
        self
    }

    fn pop(&mut self) -> char {
        self.0.pop().unwrap()
    }

    fn peak(&self) -> char {
        match self.0.last() {
            None => ' ',
            Some(c) => *c,
        }
    }
}

struct Move {
    amount: usize,
    from: usize,
    to: usize,
}

impl Move {
    fn from_str(line: &str) -> Self {
        let split: Vec<&str> = line.split(' ').collect();
        let amount = split[1].parse::<usize>().unwrap();
        let from = split[3].parse().unwrap();
        let to = split[5].parse().unwrap();
        Self { amount, from, to }
    }
}

#[derive(Debug)]
struct State(Vec<Stack>);

impl State {
    fn new() -> Self {
        let data = Self(vec![
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
            Stack::new(),
        ]);
        data
    }

    fn push(&mut self, c: char, row_number: usize) {
        self.0[row_number - 1].push(c);
    }

    fn pop(&mut self, row_number: usize) -> char {
        self.0[row_number - 1].pop()
    }

    fn move_item(&mut self, from: usize, to: usize) {
        let c = self.pop(from);
        self.push(c, to);
    }

    fn move_line(&mut self, line: &str) {
        let m = Move::from_str(line);
        for _ in 0..m.amount {
            self.move_item(m.from, m.to)
        }
    }

    fn move_line2(&mut self, line: &str) {
        let m = Move::from_str(line);
        let mut items = Vec::new();
        for _ in 0..m.amount {
            items.push(self.pop(m.from));
        }
        items.iter().rev().for_each(|c| self.push(*c, m.to));
    }

    fn initialise(&mut self, data: &str) {
        data.lines().rev().for_each(|line| {
            line.chars().enumerate().for_each(|(index, c)| {
                if c.is_alphabetic() {
                    let row = (index) / 4 + 1;
                    self.push(c, row);
                }
            })
        });
    }

    fn top_text(&self) -> String {
        self.0
            .iter()
            .map(|s| s.peak().to_string())
            .reduce(|a, b| format!("{a}{b}"))
            .unwrap()
    }
}

fn main() {
    let data = fs::read_to_string("input.txt").unwrap();
    let mut iter = data.split("\n\n");
    let beginning = iter.next().unwrap();
    let moves = iter.next().unwrap();
    let mut state = State::new();
    state.initialise(beginning);
    moves.lines().for_each(|l| state.move_line(l));
    println!("answer 1: {}", state.top_text());

    let mut state = State::new();
    state.initialise(beginning);
    moves.lines().for_each(|l| state.move_line2(l));
    println!("answer 2: {}", state.top_text());
}
